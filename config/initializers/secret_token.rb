# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'e2b65a04c41a7aa7ae42287c85eca7da5aa05fbe4346f0b6851d28301f9994e2946d3d38962b2648df6e8f308dcdbbb143b6a86bd0d833a172606c5e9ffe5156'
